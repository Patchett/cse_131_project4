/* File: ast_decl.cc
 * -----------------
 * Implementation of Decl node classes.
 */
#include "ast_decl.h"
#include "ast_type.h"
#include "ast_stmt.h"
#include "semantic_analyzer.h"
#include "errors.h"
#include <string.h>

Decl::Decl(Identifier *n) :
		Node(*n->GetLocation()) {
	Assert(n != NULL);
	(id = n)->SetParent(this);
}

VarDecl::VarDecl(Identifier *n, Type *t) :
		Decl(n) {
	Assert(n != NULL && t != NULL);
	(type = t)->SetParent(this);
}

void VarDecl::PrintChildren(int indentLevel) {
	if (type)
		type->Print(indentLevel + 1);
	if (id)
		id->Print(indentLevel + 1);
}

void VarDecl::Emit() {
	//Create a local variable
	llvm::Type* llvmType = Type::getllvmType(this->getType());
	;
	if (Scp::curr->getPrev() != NULL) {
		llvm::BasicBlock* block = IRGenerator::Instance().GetBasicBlock();
		llvm::Type* llvmType = Type::getllvmType(this->getType());
		llvm::Value *lvar = new llvm::AllocaInst(llvmType, this->getIDName(), block);
		//llvm::Value *load = new llvm::LoadInst(lvar, this->getIDName(),IRGenerator::Instance().GetBasicBlock());
		//llvm::StoreInst *store = new llvm::StoreInst(load, lvar, IRGenerator::Instance().GetBasicBlock());
//		if(llvmType->isArrayTy() || llvmType->isVectorTy()){
//			llvm::StoreInst(llvm::Constant::getNullValue(llvmType), lvar, false, IRGenerator::Instance().GetBasicBlock());
//		}
		Scp::curr->insertToSymTab(this, lvar);

	} else //create a global variable
	{
		llvm::Value *gvar = new llvm::GlobalVariable(*IRGenerator::Instance().GetOrCreateModule("This_Class_Sucks"), llvmType, false, llvm::GlobalValue::ExternalLinkage,
				llvm::Constant::getNullValue(llvmType), this->getIDName());

//		TODO Other way
//		string id_name=this->id->getNameStr();
//		IRGenerator::Instance().GetOrCreateModule("This_Class_Sucks")->getOrInsertGlobal(id_name, Type::getllvmType(this->getType()));
//		IRGenerator::Instance().GetOrCreateModule("This_Class_Sucks")->getGlobalVariable(id_name)->setInitializer(llvm::Constant::getNullValue(Type::getllvmType(this->getType())));
//		Scp::curr->insertToSymTab(this, IRGenerator::Instance().GetOrCreateModule("This_Class_Sucks")->getGlobalVariable(id_name));
		Scp::curr->insertToSymTab(this, gvar);
	}
}

void FnDecl::Emit() {

	// TODO Create a new scope, handle parameter scope stuff
	llvm::StringRef func_name(this->id->getName());
	vector<llvm::Type *> param_types_vector;
	for (int i = 0; i < formals->NumElements(); i++) {
		param_types_vector.push_back(Type::getllvmType(formals->Nth(i)->getType()));
	}
	llvm::ArrayRef<llvm::Type*> param_types(param_types_vector);
	llvm::FunctionType *ft = llvm::FunctionType::get(Type::getllvmType(this->returnType), param_types, false);
	llvm::Constant *constant = IRGenerator::Instance().GetOrCreateModule("This_Class_Sucks")->getOrInsertFunction(func_name, ft);
	llvm::Function *f = llvm::cast<llvm::Function>(constant);
//	llvm::Function::arg_iterator it = f->arg_begin();
//	for(int i = 0; i < formals->NumElements(); i++) {
//		VarDecl *var = formals->Nth(i);
//		arg->setName(formals->Nth(i)->getIDName());
//		arg++; // TODO This is probably wrong. Need to properly move the iterator
//	}
	int i = 0;
	for (llvm::Function::arg_iterator itB = f->arg_begin(), itE = f->arg_end(); itB != itE; i++, itB++) {
		//VarDecl *var = formals->Nth(i);
		itB->setName(formals->Nth(i)->getIDName());
	}

	Scp::curr->insertToFnTab(this);
	Scp *fnScp = new Scp();
	fnScp->setShouldntCreateNewScope(true);
	llvm::LLVMContext *context = IRGenerator::Instance().GetContext(); // TODO Make sure new context is created at some point.
	llvm::BasicBlock *bb = llvm::BasicBlock::Create(*context, "entry", f);
	IRGenerator::Instance().SetBasicBlock(bb);
	IRGenerator::Instance().SetFunction(f);
	//for (int i = 0; i < formals->NumElements(); i++) {
	i = 0;
	for (llvm::Function::arg_iterator itB = f->arg_begin(), itE = f->arg_end(); itB != itE; i++, itB++) {
		//fnScp->insertToSymTab(formals->Nth(i));
		//added logic to create local variable for each formal
		llvm::Type* llvmType = Type::getllvmType(formals->Nth(i)->getType());
		llvm::Value *lvar = new llvm::AllocaInst(llvmType, formals->Nth(i)->getIDName(), bb);
		Scp::curr->insertToSymTab(formals->Nth(i), lvar);
		llvm::StoreInst *store = new llvm::StoreInst(itB, lvar, IRGenerator::Instance().GetBasicBlock());
		//formals->Nth(i)->Emit();
	}
	fnScp->setRetType(returnType);
	//setting the return type of the scope to the return type of the function

	// insert a block into the function
	if (body) {
		StmtBlock *block = dynamic_cast<StmtBlock*>(body);
		block->Emit();
	}

	//if(IRGenerator::Instance().GetBasicBlock()->getTerminator() == NULL) {
	//new llvm::UnreachableInst(*(IRGenerator::Instance().GetContext()), IRGenerator::Instance().GetBasicBlock());
//		 llvm::UnreachableInst *t;
//	}

	//TODO need to comment back in after implementing return
	if (strcmp(fnScp->getRetType()->getName(), "void") == 0) {
		llvm::ReturnInst::Create(*IRGenerator::Instance().GetContext(), NULL, IRGenerator::Instance().GetBasicBlock());
	}
	IRGenerator::Instance().SetFunction(NULL);
}

FnDecl::FnDecl(Identifier *n, Type *r, List<VarDecl*> *d) :
		Decl(n) {
	Assert(n != NULL && r!= NULL && d != NULL);
	(returnType = r)->SetParent(this);
	(formals = d)->SetParentAll(this);
	body = NULL;
}

void FnDecl::SetFunctionBody(Stmt *b) {
	(body = b)->SetParent(this);
}

void FnDecl::PrintChildren(int indentLevel) {
	if (returnType)
		returnType->Print(indentLevel + 1, "(return type) ");
	if (id)
		id->Print(indentLevel + 1);
	if (formals)
		formals->PrintAll(indentLevel + 1, "(formals) ");
	if (body)
		body->Print(indentLevel + 1, "(body) ");
}

