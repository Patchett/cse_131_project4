int switchtest(int b)
{ 
	int x;
	x = 0;
    switch (b) {
        case 1: break;
        case 2: x = 2; break;
        case 3: x = 3; break;
        case 4: 
        case 5: x = 100; break;
        //default: x =  300;
    }

    return x;
}