/**
 * File: irgen.h
 * ----------- 
 *  This file defines a class for LLVM IR Generation.
 *
 *  All LLVM instruction related functions or utilities can be implemented
 *  here. You'll need to customize this class heavily to provide any helpers
 *  or untility as you need.
 */

#ifndef _H_IRGen
#define _H_IRGen

// LLVM headers
#include "llvm/IR/Module.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Constants.h"
#include <string>

using std::string;

class IRGenerator {
public:
	static IRGenerator &Instance() {
		static IRGenerator irgen;
		return irgen;
	}

	llvm::Module *GetOrCreateModule(const char *moduleID);
	llvm::LLVMContext *GetContext() const {
		return context;
	}
	// Add your helper functions here
	llvm::Function *GetFunction() const;
	void SetFunction(llvm::Function *func);
	llvm::BasicBlock *GetBasicBlock() const;
	void SetBasicBlock(llvm::BasicBlock *bb);
	llvm::Type *GetIntType() const;
	llvm::Type *GetBoolType() const;
	llvm::Type *GetFloatType() const;

	// ======================================== Our Functions ======================================== //
	llvm::BasicBlock *createNewBasicBlock(string name) { return llvm::BasicBlock::Create(*context, name, GetFunction()); }
	llvm::Type *GetVoidType() const { return llvm::Type::getVoidTy(*IRGenerator::Instance().GetContext()); }
	llvm::Type *GetVec2Type() const { return llvm::VectorType::get(GetFloatType(),2); }
	llvm::Type *GetVec3Type() const { return llvm::VectorType::get(GetFloatType(),3); }
	llvm::Type *GetVec4Type() const { return llvm::VectorType::get(GetFloatType(),4); }
	llvm::Value *GetIntConst(int n) const { return llvm::ConstantInt::get(GetIntType(), n); }
	llvm::Value *GetFloatConst(float n) const { return llvm::ConstantInt::get(GetFloatType(), n);  }
	llvm::Value *branchToBlock(llvm::BasicBlock *bb);
	llvm::Value *getVecType(llvm::Value *left_value, llvm::Value *right_value);
	llvm::CmpInst::Predicate getGenericPredicate(llvm::Type *type, const string op);
	llvm::CmpInst::Predicate getFloatPredicate(string op);
	llvm::CmpInst::Predicate getIntPredicate(string op);
	llvm::CmpInst *convertExprToLLVMCmp(llvm::Value *left, const string op, llvm::Value *right);
	llvm::Instruction::BinaryOps getLLVMBinaryOperator(llvm::Type *t, const char op);
	llvm::Instruction::BinaryOps getLLVMFloatOperator(const char op);
	llvm::Instruction::BinaryOps getLLVMIntOperator(const char op);
	llvm::Value *exprToVector(llvm::Value *left_value, llvm::Value *right_value);
	void createGenericVecElement(llvm::Value *left_or_right, llvm::Value *vector, llvm::Value *int_const);
	int swizzleToInt(char swizzle);
	llvm::Value *performLLVMArithmeticOp(llvm::Value *left, llvm::Instruction::BinaryOps op, llvm::Value *right);


	llvm::BasicBlock* getEndBb() const {
		return endBB;
	}

	void setEndBb(llvm::BasicBlock* endBb) {
		endBB = endBb;
	}

	llvm::BasicBlock* getStartBb() const {
		return startBB;
	}

	void setStartBb(llvm::BasicBlock* startBb) {
		startBB = startBb;
	}
	llvm::SwitchInst* getSwitchInst() const {
		return switchInst;
	}

	void setSwitchBb(llvm::SwitchInst* switchI) {
		switchInst = switchI;
	}
private:
	static IRGenerator ig;
	IRGenerator();
	IRGenerator(IRGenerator const&); // ensure singleton instance can't be copied
	void operator=(IRGenerator const&); // ""
	llvm::LLVMContext *context;
	llvm::Module *module;
	// track which function or basic block is active
	llvm::Function *currentFunc;
	llvm::BasicBlock *currentBB;
	llvm::BasicBlock *endBB;
	llvm::BasicBlock *startBB;
	llvm::SwitchInst *switchInst;
	static const char *TargetTriple;
	static const char *TargetLayout;
};

#endif
