/* irgen.cc -  LLVM IR generator
 *
 * You can implement any LLVM related functions here.
 */

#include "irgen.h"

IRGenerator::IRGenerator() :
		context(NULL), module(NULL), currentFunc(NULL), currentBB(NULL) {
}

llvm::Module *IRGenerator::GetOrCreateModule(const char *moduleID) {
	if (module == NULL) {
		context = new llvm::LLVMContext();
		module = new llvm::Module(moduleID, *context);
		module->setTargetTriple(TargetTriple);
		module->setDataLayout(TargetLayout);
	}
	return module;
}

void IRGenerator::SetFunction(llvm::Function *func) {
	currentFunc = func;
}

llvm::Function *IRGenerator::GetFunction() const {
	return currentFunc;
}

void IRGenerator::SetBasicBlock(llvm::BasicBlock *bb) {
	currentBB = bb;
}

llvm::BasicBlock *IRGenerator::GetBasicBlock() const {
	return currentBB;
}

llvm::Type *IRGenerator::GetIntType() const {
	llvm::Type *ty = llvm::Type::getInt32Ty(*context);
	return ty;
}

llvm::Type *IRGenerator::GetBoolType() const {
	llvm::Type *ty = llvm::Type::getInt1Ty(*context);
	return ty;
}

llvm::Type *IRGenerator::GetFloatType() const {
	llvm::Type *ty = llvm::Type::getFloatTy(*context);
	return ty;
}

llvm::CmpInst::Predicate IRGenerator::getGenericPredicate(llvm::Type *type, const string op) {
	if (type->isFPOrFPVectorTy()) {
		return getFloatPredicate(op);
	} else {
		return getIntPredicate(op);
	}
}

llvm::CmpInst *IRGenerator::convertExprToLLVMCmp(llvm::Value *left, const string op, llvm::Value *right) {
	if (!(right->getType()->isFPOrFPVectorTy())) {
		return new llvm::ICmpInst(*currentBB, getGenericPredicate(right->getType(), op), left, right, "dude_compare");
	} else {
		return new llvm::FCmpInst(*currentBB, getGenericPredicate(right->getType(), op), left, right, "dude_compare");
	}
}

llvm::CmpInst::Predicate IRGenerator::getIntPredicate(string op) {
	if ("<" == op) {
		return llvm::ICmpInst::ICMP_SLT;
	} else if ("<=" == op) {
		return llvm::ICmpInst::ICMP_SLE;
	} else if (">" == op) {
		return llvm::ICmpInst::ICMP_SGT;
	} else if (">=" == op) {
		return llvm::ICmpInst::ICMP_SGE;
	} else if ("==" == op) {
		return llvm::ICmpInst::ICMP_EQ;
	} else if ("!=" == op) {
		return llvm::ICmpInst::ICMP_NE;
	} else {
		return llvm::ICmpInst::BAD_ICMP_PREDICATE;
	}
}

llvm::CmpInst::Predicate IRGenerator::getFloatPredicate(string op) {
	if ("<" == op) {
		return llvm::ICmpInst::FCMP_OLT;
	} else if ("<=" == op) {
		return llvm::ICmpInst::FCMP_OLE;
	} else if (">" == op) {
		return llvm::ICmpInst::FCMP_OGT;
	} else if (">=" == op) {
		return llvm::ICmpInst::FCMP_OGE;
	} else if ("==" == op) {
		return llvm::ICmpInst::FCMP_OEQ;
	} else if ("!=" == op) {
		return llvm::ICmpInst::FCMP_ONE;
	} else {
		return llvm::ICmpInst::BAD_ICMP_PREDICATE;
	}
}

llvm::Value *IRGenerator::branchToBlock(llvm::BasicBlock *bb) {
	if (NULL == GetBasicBlock()->getTerminator()) {
		return llvm::BranchInst::Create(bb, GetBasicBlock());
	} else {
		return NULL;
	}
}

llvm::Instruction::BinaryOps IRGenerator::getLLVMBinaryOperator(llvm::Type *t, const char op) {
	if (t->isFPOrFPVectorTy()) {
		return getLLVMFloatOperator(op);
	} else {
		return getLLVMIntOperator(op);
	}
}

void IRGenerator::createGenericVecElement(llvm::Value *float_val, llvm::Value *vector, llvm::Value *int_const) {
	llvm::Value *load_inst = new llvm::LoadInst(vector, "blonde_bandit_vec2", GetBasicBlock());
	llvm::Value *vec_element = llvm::InsertElementInst::Create(load_inst, float_val, int_const, "", GetBasicBlock());
	llvm::StoreInst(load_inst, vector, false, GetBasicBlock());
}

llvm::Value *IRGenerator::exprToVector(llvm::Value *left_value, llvm::Value *right_value) {
	llvm::Value *vector = NULL;
	if (left_value->getType() == GetVec2Type() && right_value->getType() == IRGenerator::Instance().GetFloatType()) {
		vector = new llvm::AllocaInst(GetVec2Type(), "blonde_bandit_vec2", GetBasicBlock());
		llvm::StoreInst(llvm::Constant::getNullValue(GetVec2Type()), vector, false, GetBasicBlock());
		createGenericVecElement(right_value, vector, GetIntConst(0));
		createGenericVecElement(right_value, vector, GetIntConst(1));
	} else if (left_value->getType() == IRGenerator::Instance().GetFloatType() && right_value->getType() == GetVec2Type()) {
		vector = new llvm::AllocaInst(GetVec2Type(), "blonde_bandit_vec2", GetBasicBlock());
		llvm::StoreInst(llvm::Constant::getNullValue(GetVec2Type()), vector, false, GetBasicBlock());
		createGenericVecElement(left_value, vector, GetIntConst(0));
		createGenericVecElement(left_value, vector, GetIntConst(1));
	} else if (left_value->getType() == GetVec3Type() && right_value->getType() == IRGenerator::Instance().GetFloatType()) {
		vector = new llvm::AllocaInst(GetVec3Type(), "blonde_bandit_vec3", GetBasicBlock());
		llvm::StoreInst(llvm::Constant::getNullValue(GetVec3Type()), vector, false, GetBasicBlock());
		createGenericVecElement(right_value, vector, GetIntConst(0));
		createGenericVecElement(right_value, vector, GetIntConst(1));
		createGenericVecElement(right_value, vector, GetIntConst(2));
	} else if (left_value->getType() == IRGenerator::Instance().GetFloatType() && right_value->getType() == GetVec3Type()) {
		vector = new llvm::AllocaInst(GetVec3Type(), "blonde_bandit_vec3", GetBasicBlock());
		llvm::StoreInst(llvm::Constant::getNullValue(GetVec3Type()), vector, false, GetBasicBlock());
		createGenericVecElement(left_value, vector, GetIntConst(0));
		createGenericVecElement(left_value, vector, GetIntConst(1));
		createGenericVecElement(left_value, vector, GetIntConst(2));
	} else if (left_value->getType() == GetVec4Type() && right_value->getType() == IRGenerator::Instance().GetFloatType()) {
		vector = new llvm::AllocaInst(GetVec4Type(), "blonde_bandit_vec4", GetBasicBlock());
		llvm::StoreInst(llvm::Constant::getNullValue(GetVec4Type()), vector, false, GetBasicBlock());
		createGenericVecElement(right_value, vector, GetIntConst(0));
		createGenericVecElement(right_value, vector, GetIntConst(1));
		createGenericVecElement(right_value, vector, GetIntConst(2));
		createGenericVecElement(right_value, vector, GetIntConst(3));
	} else if (left_value->getType() == IRGenerator::Instance().GetFloatType() && right_value->getType() == GetVec4Type()) {
		vector = new llvm::AllocaInst(GetVec4Type(), "blonde_bandit_vec4", GetBasicBlock());
		llvm::StoreInst(llvm::Constant::getNullValue(GetVec4Type()), vector, false, GetBasicBlock());
		createGenericVecElement(left_value, vector, GetIntConst(0));
		createGenericVecElement(left_value, vector, GetIntConst(1));
		createGenericVecElement(left_value, vector, GetIntConst(2));
		createGenericVecElement(left_value, vector, GetIntConst(3));
	}
	return vector;
}

llvm::Instruction::BinaryOps IRGenerator::getLLVMIntOperator(const char op) {
	if (op == '+') {
		return llvm::Instruction::FAdd;
	} else if (op == '-') {
		return llvm::Instruction::FSub;
	} else if (op == '/') {
		return llvm::Instruction::FDiv;
	} else if (op == '*') {
		return llvm::Instruction::FMul;
	} else {
		return llvm::Instruction::FAdd;
	}
}

llvm::Instruction::BinaryOps IRGenerator::getLLVMFloatOperator(const char op) {
	if (op == '+') {
		return llvm::Instruction::Add;
	} else if (op == '-') {
		return llvm::Instruction::Sub;
	} else if (op == '/') {
		return llvm::Instruction::SDiv;
	} else if (op == '*') {
		return llvm::Instruction::Mul;
	} else {
		return llvm::Instruction::Add;
	}
}

int IRGenerator::swizzleToInt(char swizzle) {
	if(swizzle == 'x') {
		return 0;
	} else if (swizzle == 'y') {
		return 1;
	} else if (swizzle == 'z') {
		return 2;
	} else /*swizzle == 'w'*/{
		return 3;
	}
}

llvm::Value *IRGenerator::performLLVMArithmeticOp(llvm::Value *left, llvm::Instruction::BinaryOps op, llvm::Value *right) {
	if(left->getType() == GetFloatType() && right->getType()->isVectorTy()){
		left = exprToVector(left,right);
		left = new llvm::LoadInst(left, "dude_load_vec", GetBasicBlock());
	}
	else if(left->getType()->isVectorTy() && right->getType() == GetFloatType()){
		right = exprToVector(left,right);
		right = new llvm::LoadInst(left, "dude_load_vec", GetBasicBlock());
	}
	return llvm::BinaryOperator::Create(op, left, right, "dude_arithmetic", GetBasicBlock());
}

// TODO switch to performLLVMArithmeticOp
const char *IRGenerator::TargetLayout =
		"e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128";

const char *IRGenerator::TargetTriple = "x86_64-redhat-linux-gnu";

