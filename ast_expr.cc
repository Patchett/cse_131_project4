/* File: ast_expr.cc
 * -----------------
 * Implementation of expression node classes.
 */

#include <string.h>
#include "ast_expr.h"
#include "ast_type.h"
#include "ast_decl.h"
#include <string>

IntConstant::IntConstant(yyltype loc, int val) :
		Expr(loc) {
	value = val;
}
void IntConstant::PrintChildren(int indentLevel) {
	printf("%d", value);
}
llvm::Value* IntConstant::EmitExpr() {

	string valStr = std::to_string(this->getValue());
	llvm::Value *val = llvm::ConstantInt::get(Type::getllvmType(this->getExprType()), llvm::APInt(32, llvm::StringRef(valStr), 10));
	return val;
}

FloatConstant::FloatConstant(yyltype loc, double val) :
		Expr(loc) {
	value = val;
}
void FloatConstant::PrintChildren(int indentLevel) {
	printf("%g", value);
}
llvm::Value* FloatConstant::EmitExpr() {

	string valStr = std::to_string(this->getValue());
	llvm::Value *val = llvm::ConstantFP::get(Type::getllvmType(this->getExprType()), llvm::StringRef(valStr));
	return val;
}

BoolConstant::BoolConstant(yyltype loc, bool val) :
		Expr(loc) {
	value = val;
}
void BoolConstant::PrintChildren(int indentLevel) {
	printf("%s", value ? "true" : "false");
}

VarExpr::VarExpr(yyltype loc, Identifier *ident) :
		Expr(loc) {
	Assert(ident != NULL);
	this->id = ident;
}

void VarExpr::PrintChildren(int indentLevel) {
	id->Print(indentLevel + 1);
}

llvm::Value* VarExpr::EmitExpr() {
	string keyStr = this->getIdentifier()->getNameStr();
	llvm::Value* val = Scp::curr->getVarValue(keyStr);
	if (val) {
		llvm::Value *load = new llvm::LoadInst(val, keyStr, IRGenerator::Instance().GetBasicBlock());
		return load;
	} else {
		return NULL;
	}
}

Operator::Operator(yyltype loc, const char *tok) :
		Node(loc) {
	Assert(tok != NULL);
	strncpy(tokenString, tok, sizeof(tokenString));
}

void Operator::PrintChildren(int indentLevel) {
	printf("%s", tokenString);
}

CompoundExpr::CompoundExpr(Expr *l, Operator *o, Expr *r) :
		Expr(Join(l->GetLocation(), r->GetLocation())) {
	Assert(l != NULL && o != NULL && r != NULL);
	(op = o)->SetParent(this);
	(left = l)->SetParent(this);
	(right = r)->SetParent(this);
}

CompoundExpr::CompoundExpr(Operator *o, Expr *r) :
		Expr(Join(o->GetLocation(), r->GetLocation())) {
	Assert(o != NULL && r != NULL);
	left = NULL;
	(op = o)->SetParent(this);
	(right = r)->SetParent(this);
}

CompoundExpr::CompoundExpr(Expr *l, Operator *o) :
		Expr(Join(l->GetLocation(), o->GetLocation())) {
	Assert(l != NULL && o != NULL);
	(left = l)->SetParent(this);
	(op = o)->SetParent(this);
}

void CompoundExpr::PrintChildren(int indentLevel) {
	if (left)
		left->Print(indentLevel + 1);
	op->Print(indentLevel + 1);
	if (right)
		right->Print(indentLevel + 1);
}

llvm::Value* ArithmeticExpr::EmitExpr() {
	string arith_operator_str = op->getTokStr();
	int delta = 1;
	if (NULL == left) { // prefix unary expression
		llvm::Value *right_value = right->EmitExpr();
		llvm::Instruction::BinaryOps llvm_op = IRGenerator::Instance().getLLVMBinaryOperator(right_value->getType(), arith_operator_str[0]);
		if (op->getTokStr().size() == 1) { // its a negative number or positive number
			delta = 0;
		}
		llvm::Value *int_or_fp_value = NULL;
		if (right_value->getType()->isFPOrFPVectorTy()) { // floating point
			int_or_fp_value = llvm::ConstantFP::get(llvm::Type::getFloatTy(*IRGenerator::Instance().GetContext()), delta);
		} else { // integer
			int_or_fp_value = llvm::ConstantInt::get(llvm::Type::getInt32Ty(*IRGenerator::Instance().GetContext()), delta);
		}
		if (delta == 0) {
			return IRGenerator::Instance().performLLVMArithmeticOp(int_or_fp_value,llvm_op,right_value);
//			return llvm::BinaryOperator::Create(llvm_op, int_or_fp_value, right_value, "dude_unary_arith", IRGenerator::Instance().GetBasicBlock());
		} else {
			return IRGenerator::Instance().performLLVMArithmeticOp(right_value,llvm_op,int_or_fp_value);
//			return llvm::BinaryOperator::Create(llvm_op, right_value, int_or_fp_value, "dude_unary_arith", IRGenerator::Instance().GetBasicBlock());
		}
	} else { // binary expression
		// TODO deal with vec/mat edge cases eg vec2+float or float+vec2
		llvm::Value *left_value = left->EmitExpr();
		llvm::Value *right_value = right->EmitExpr();
		llvm::Instruction::BinaryOps llvm_op = IRGenerator::Instance().getLLVMBinaryOperator(right_value->getType(), arith_operator_str[0]);
		return IRGenerator::Instance().performLLVMArithmeticOp(left_value,llvm_op,right_value);
//		return llvm::BinaryOperator::Create(llvm_op, left_value, right_value, "dude_binary_arith", IRGenerator::Instance().GetBasicBlock());
	}
	//		if (dynamic_cast<IntConstant*>(right) != NULL)
	//		{
	//			if (dynamic_cast<IntConstant*>(left) != NULL)
	//			{
	//				IntConstant* r = dynamic_cast<IntConstant*>(right);
	//				string valRStr = std::to_string(r->getValue());
	//				IntConstant* l = dynamic_cast<IntConstant*>(left);
	//				string valLStr = std::to_string(l->getValue());
	//
	//				//llvm::ConstantInt* constRInt = llvm::ConstantInt::get(*IRGenerator::Instance().GetContext(), llvm::APInt(32, llvm::StringRef(valRStr), 10));
	//				//llvm::ConstantInt* constLInt = llvm::ConstantInt::get(*IRGenerator::Instance().GetContext(), llvm::APInt(32, llvm::StringRef(valLStr), 10));
	//				llvm::Value *rval = llvm::ConstantInt::get(Type::getllvmType(right->getExprType()), llvm::APInt(32, llvm::StringRef(valRStr), 10));
	//				llvm::Value *lval = llvm::ConstantInt::get(Type::getllvmType(left->getExprType()), llvm::APInt(32, llvm::StringRef(valLStr), 10));
	//				llvm::BinaryOperator* add = llvm::BinaryOperator::Create(llvm::Instruction::Add, rval, lval, "add", IRGenerator::Instance().GetBasicBlock());
	//				//ConstantInt* const_int32_1 = llvm::ConstantInt::get(*IRGenerator::Instance().GetContext(), APInt(32, StringRef("7"), 10));
	//				return add;
	//			}
	//			else if (dynamic_cast<VarExpr*>(left) != NULL)
	//			{
	//				IntConstant* r = dynamic_cast<IntConstant*>(right);
	//				string valRStr = std::to_string(r->getValue());
	//				VarExpr* l = dynamic_cast<VarExpr*>(left);
	//				string keyLStr = l->getIdentifier()->getNameStr();
	//				llvm::Value* lval= Scp::curr->getVarValue(keyLStr);
	//
	//				llvm::Value *loadL = new llvm::LoadInst(lval, keyLStr,IRGenerator::Instance().GetBasicBlock());
	//				llvm::Value *rval = llvm::ConstantInt::get(Type::getllvmType(right->getExprType()), llvm::APInt(32, llvm::StringRef(valRStr), 10));
	//				llvm::BinaryOperator* add = llvm::BinaryOperator::Create(llvm::Instruction::Add, rval, loadL, "add", IRGenerator::Instance().GetBasicBlock());
	//				return add;
	//			}
	//		}
	//		return llvm::BinaryOperator::CreateAdd(rval, lval,
	//				"hello", IRGenerator::Instance().GetBasicBlock());
}

ArrayAccess::ArrayAccess(yyltype loc, Expr *b, Expr *s) :
		LValue(loc) {
	(base = b)->SetParent(this);
	(subscript = s)->SetParent(this);
}

void ArrayAccess::PrintChildren(int indentLevel) {
	base->Print(indentLevel + 1);
	subscript->Print(indentLevel + 1, "(subscript) ");
}

FieldAccess::FieldAccess(Expr *b, Identifier *f) :
		LValue(b ? Join(b->GetLocation(), f->GetLocation()) : *f->GetLocation()) {
	Assert(f != NULL); // b can be be NULL (just means no explicit base)
	base = b;
	if (base)
		base->SetParent(this);
	(field = f)->SetParent(this);
}

void FieldAccess::PrintChildren(int indentLevel) {
	if (base)
		base->Print(indentLevel + 1);
	field->Print(indentLevel + 1);
}

llvm::Value* FieldAccess::EmitExpr() {
	llvm::Value *base_value = base->EmitExpr();
	string swizzle = field->getNameStr();
//	(base_value, swizzle)
	llvm::Value *swizzle_pos = IRGenerator::Instance().GetIntConst(IRGenerator::Instance().swizzleToInt(swizzle[0]));
	if (swizzle.length() == 1) {
		return llvm::ExtractElementInst::Create(base_value, swizzle_pos, swizzle, IRGenerator::Instance().GetBasicBlock());
	} else {
		llvm::Constant *mask = NULL;
		std::vector<llvm::Constant*> buckets;
		for (unsigned int i = swizzle.length(); i > 0; i--) {
			buckets.push_back((llvm::Constant*) IRGenerator::Instance().GetIntConst(IRGenerator::Instance().swizzleToInt(swizzle[i - 1])));
		}
		mask = llvm::ConstantVector::get(buckets);
		return new llvm::ShuffleVectorInst(base_value, llvm::UndefValue::get(base_value->getType()), mask, "dude_suffleVec", IRGenerator::Instance().GetBasicBlock());
	}
}

llvm::Value* AssignExpr::EmitExpr() {
	string assign_operator_str = op->getTokStr();
	llvm::Value * left_value = left->EmitExpr();
	llvm::Value * right_value = right->EmitExpr();
//	if (right_value->getType() == IRGenerator::Instance().GetFloatType() && left_value->getType()->isVectorTy()) { // vector stuff
//
//	}
	if (assign_operator_str != "=") {
		llvm::Instruction::BinaryOps bin_op = IRGenerator::Instance().getLLVMBinaryOperator(left_value->getType(), assign_operator_str[0]);
		right_value = llvm::BinaryOperator::Create(bin_op, left_value, right_value, "dude_assign_expr", IRGenerator::Instance().GetBasicBlock());
	}
	left->LLVMstore(right_value);
	return right_value;
}

llvm::Value *PostfixExpr::EmitExpr() {
	llvm::Value *int_or_fp_value = NULL;
	llvm::Value *left_value = left->EmitExpr();
	string arith_operator_str = op->getTokStr();
	llvm::Instruction::BinaryOps llvm_op = IRGenerator::Instance().getLLVMBinaryOperator(left_value->getType(), arith_operator_str[0]);
	if (left_value->getType()->isFPOrFPVectorTy()) { // floating point
		int_or_fp_value = llvm::ConstantFP::get(llvm::Type::getFloatTy(*IRGenerator::Instance().GetContext()), 1.0);
	} else { // integer
		int_or_fp_value = llvm::ConstantInt::get(llvm::Type::getInt32Ty(*IRGenerator::Instance().GetContext()), 1);
	}
	llvm::Value *postfix_value = IRGenerator::Instance().performLLVMArithmeticOp(left_value, llvm_op, int_or_fp_value);
	left->LLVMstore(postfix_value);
	return left_value;
}

llvm::Value* CompoundExpr::EmitExpr() {
	// cout << "Starting Compound::CheckExpr()\n";
	Type *l_type = NULL;
	Type *r_type = NULL;

	if (left) {
		l_type = left->getExprType();
	}
	if (right) {
		r_type = right->getExprType();
		// cout << "r_type is " << r_type << " in Compound::CheckExpr()\n";
	}
	if (NULL == l_type) {
		// cout << "l_type is null in Compound::CheckExpr()\n";
		if (isValidUnaryExpr(r_type)) {
			//return r_type;
			return NULL; //TODO
		} else {
			ReportError::IncompatibleOperand(op, r_type);
			// return new Type("error");
			return NULL; //TODO
		}
	} else if (NULL == r_type) {
		// cout << "r_type is null in Compound::CheckExpr()\n";
		if (isValidUnaryExpr(l_type)) {
			// return l_type;
			return NULL; //TODO
		} else {
			ReportError::IncompatibleOperandLHS(op, l_type);
			//return new Type("error");
			return NULL; //TODO
		}
	} else {
		// cout << "l_type and r_type are not null in Compound::CheckExpr()\n";
		Type *t = isValidFullExpr(l_type, op, r_type);
		if (NULL != t) {
			// return t;
			return NULL; //TODO
		} else {
			ReportError::IncompatibleOperands(op, l_type, r_type);
			//return new Type("error");
			return NULL; //TODO
		}
	}
}

Call::Call(yyltype loc, Expr *b, Identifier *f, List<Expr*> *a) :
		Expr(loc) {
	Assert(f != NULL && a != NULL); // b can be be NULL (just means no explicit base)
	base = b;
	if (base)
		base->SetParent(this);
	(field = f)->SetParent(this);
	(actuals = a)->SetParentAll(this);
}

void Call::PrintChildren(int indentLevel) {
	if (base)
		base->Print(indentLevel + 1);
	if (field)
		field->Print(indentLevel + 1);
	if (actuals)
		actuals->PrintAll(indentLevel + 1, "(actuals) ");
}

Type *isValidFullExpr(Type *t1, Operator *op, Type *t2) {
	if (isArithOp(op) || isAssignOp(op)) {
		return checkAssignOrArithOp(t1, op, t2);
	} else if (isRelOp(op)) {
		if (checkRelOp(t1, t2)) {
			return new Type("bool");
		} else {
			return NULL;
		}
	} else if (isLogicOp(op)) {
		if (checkLogicOp(t1, t2)) {
			return new Type("bool");
		} else {
			return NULL;
		}
	} else if (isEqOp(op)) {
		if (checkEqOp(t1, t2)) {
			return new Type("bool");
		} else {
			return NULL;
		}
	} else {
		return NULL;
	}
}

Type *checkAssignOrArithOp(Type *t1, Operator *op, Type *t2) {
	if (op->getTokStr() == "=") {
		// cout << "Found = operator in checkAssignOrArithOp" << endl;
		if ((t1->getNameStr() == t2->getNameStr())) {
			return t1;
		} else {
			return NULL;
		}
	} else if (op->getTokStr() == "*=") {
		if (checkVectorType(t1)) {
			if ((t2->getNameStr().substr(3, 1) == t1->getNameStr().substr(3, 1)) || t2->getNameStr() == "float") {
				return t1;
			} else {
				return NULL;
			}
		} else if (checkMatType(t1)) {
			if ((t2->getNameStr() == "float") || (t1->getNameStr() == t2->getNameStr())) {
				return t1;
			} else {
				return NULL;
			}
		} else if (checkScalarType(t1)) {
			if (t1->getNameStr() == t2->getNameStr()) {
				return t1;
			} else {
				return NULL;
			}
		} else {
			return NULL;
		}
	} else if (op->getTokStr() == "*") {
		if (checkVectorType(t1)) {
			if (checkMatType(t2)) {
				if ((t2->getNameStr().substr(3, 1) == t1->getNameStr().substr(3, 1))) {
					return t1;
				} else {
					return NULL;
				}
			} else if ((t2->getNameStr() == "float") || (t1->getNameStr() == t2->getNameStr())) {
				return t1;
			} else {
				return NULL;
			}
		} else if (checkMatType(t1)) {
			if (checkVectorType(t2)) {
				if (t2->getNameStr().substr(3, 1) == t1->getNameStr().substr(3, 1)) {
					return t2;
				} else {
					return NULL;
				}
			} else if ((t2->getNameStr() == "float") || (t1->getNameStr() == t2->getNameStr())) {
				return t1;
			} else {
				return NULL;
			}
		} else if (checkScalarType(t1)) {
			if (t1->getNameStr() == t2->getNameStr()) {
				return t1;
			} else if (checkMatType(t2) || checkVectorType(t2)) {
				return t2;
			} else {
				return NULL;
			}
		} else {
			return NULL;
		}
	} else {
		if (checkMatType(t1) || checkVectorType(t1)) {
			if ((t2->getNameStr() == "float") || (t1->getNameStr() == t2->getNameStr())) {
				return t1;
			} else {
				return NULL;
			}
		} else if (checkMatType(t2) || checkVectorType(t2)) {
			if ((t1->getNameStr() == "float") && (op->getTokStr().find("=") != string::npos)) {
				return NULL;
			} else if ((t1->getNameStr() == "float") && (op->getTokStr().find("=") == string::npos)) {
				return t2;
			} else if (t1->getNameStr() == t2->getNameStr()) {
				return t1;
			} else {
				return NULL;
			}
		} else if (checkScalarType(t1)) {
			if (t1->getNameStr() == t2->getNameStr()) {
				return t1;
			} else {
				return NULL;
			}
		} else {
			return NULL;
		}
	}
}

bool checkLogicOp(Type *t1, Type *t2) {
	return checkBoolType(t1) && checkBoolType(t2);
}

bool checkEqOp(Type *t1, Type *t2) {
	return (t1->getNameStr() == t2->getNameStr());
}

bool checkRelOp(Type *t1, Type *t2) {
	// cout << "checkRelOp returned " << (checkScalarType(t1) && (t1->getNameStr() == t2->getNameStr())) << endl;
	return (checkScalarType(t1) && (t1->getNameStr() == t2->getNameStr()));
}

bool checkArithOp(Type *t1, Type *t2) {
	if (checkVectorType(t1) || checkMatType(t1)) {
		// cout << "checkArithOp returned " << (t2->getNameStr() == "float" || (t2->getNameStr().substr(3, 1) == t1->getNameStr().substr(3, 1))) << endl;
		return (t2->getNameStr() == "float" || (t2->getNameStr().substr(3, 1) == t1->getNameStr().substr(3, 1)));
	} else if (t1->getNameStr() == "int") {
		// cout << "checkArithOp returned " << (t2->getNameStr() == "int") << endl;
		return t2->getNameStr() == "int";
	} else if (t1->getNameStr() == "float") {
		// cout << "checkArithOp returned " << ((t2->getNameStr() == "float") || checkMatType(t2) || checkVectorType(t2)) << endl;
		return ((t2->getNameStr() == "float") || checkMatType(t2) || checkVectorType(t2));
	} else {
		return false;
	}
}

bool isArithOp(Operator *op) {
	return ((op->getTokStr() == "+") || (op->getTokStr() == "-") || (op->getTokStr() == "*") || (op->getTokStr() == "/"));
}

bool isAssignOp(Operator *op) {
	return op->getTokStr() == "=" || op->getTokStr() == "*=" || op->getTokStr() == "/=" || op->getTokStr() == "-=" || op->getTokStr() == "+=";
}

bool isRelOp(Operator *op) {
	return op->getTokStr() == "<" || op->getTokStr() == ">" || op->getTokStr() == ">=" || op->getTokStr() == "<=";
}

bool isLogicOp(Operator *op) {
	return op->getTokStr() == "&&" || op->getTokStr() == "||";
}

bool isEqOp(Operator *op) {
	return op->getTokStr() == "!=" || op->getTokStr() == "==";
}

bool isValidUnaryExpr(Type *t) {
	return checkVectorType(t) || checkScalarType(t) || checkMatType(t);
}

bool checkBoolType(Type *t) {
	return t->getNameStr() == "bool";
}

bool checkMatType(Type *t) {
	return t->getNameStr() == "mat2" || t->getNameStr() == "mat3" || t->getNameStr() == "mat4";
}

bool checkVectorType(Type *t) {
	return t->getNameStr() == "vec2" || t->getNameStr() == "vec3" || t->getNameStr() == "vec4";
}

bool checkScalarType(Type *t) {
	return t->getNameStr() == "int" || t->getNameStr() == "float";
}

