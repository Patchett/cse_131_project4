/* File: ast_type.h
 * ----------------
 * In our parse tree, Type nodes are used to represent and
 * store type information. The base Type class is used
 * for built-in types, the NamedType for classes and interfaces,
 * and the ArrayType for arrays of other types.  
 *
 * pp4: You will need to extend the Type classes to implement
 * mapping between LLVM type system and parser/AST type system.
 */

#ifndef _H_ast_type
#define _H_ast_type

#include "ast.h"
#include "list.h"
#include "irgen.h"

class Type: public Node {
protected:
	char *typeName;

public:
	static Type *intType, *floatType, *boolType, *voidType, *vec2Type, *vec3Type, *vec4Type, *mat2Type, *mat3Type, *mat4Type, *errorType;

	Type(yyltype loc) :
			Node(loc) {
	}
	Type(const char *str);
	char *getName() {
		return typeName;
	}
	string getNameStr() {
		return string(typeName);
	}
	virtual void PrintToStream(ostream &out) {
		out << typeName;
	}
	friend ostream &operator<<(ostream &out, Type *t) {
		t->PrintToStream(out);
		return out;
	}
	const char *GetPrintNameForNode() {
		return "Type";
	}
	void PrintChildren(int indentLevel);
	static llvm::Type *getllvmType(Type *t) {
		if (t->getNameStr() == "bool") {
			return IRGenerator::Instance().GetBoolType();
		} else if (t->getNameStr() == "int") {
			return IRGenerator::Instance().GetIntType();
		} else if (t->getNameStr() == "float") {
			return IRGenerator::Instance().GetFloatType();
		} else if (t->getNameStr() == "void") {
			return IRGenerator::Instance().GetVoidType();
		} else if (t->getNameStr() == "vec2") {
			return IRGenerator::Instance().GetVec2Type();
		} else if (t->getNameStr() == "vec3") {
			return IRGenerator::Instance().GetVec3Type();
		} else if (t->getNameStr() == "vec4") {
			return IRGenerator::Instance().GetVec4Type();
		}
		return IRGenerator::Instance().GetIntType();
	}
};

class NamedType: public Type {
protected:
	Identifier *id;

public:
	NamedType(Identifier *i);

	const char *GetPrintNameForNode() {
		return "NamedType";
	}
	void PrintChildren(int indentLevel);
};

class ArrayType: public Type {
protected:
	Type *elemType;

public:
	ArrayType(yyltype loc, Type *elemType);

	const char *GetPrintNameForNode() {
		return "ArrayType";
	}
	void PrintChildren(int indentLevel);
};

#endif
