/* File: ast_stmt.cc
 * -----------------
 * Implementation of statement node classes.
 */
#include "ast_stmt.h"
#include "ast_type.h"
#include "ast_decl.h"
#include "ast_expr.h"
#include "errors.h"
#include <string.h>
//#include "ast.h"
//#include "list.h"
//#include "semantic_analyzer.h"

#include "irgen.h"
#include "llvm/Bitcode/ReaderWriter.h"
#include "llvm/Support/raw_ostream.h"

Program::Program(List<Decl*> *d) {
	Assert(d != NULL);
	(decls = d)->SetParentAll(this);
}

void Program::PrintChildren(int indentLevel) {
	decls->PrintAll(indentLevel + 1);
	printf("\n");
}

void Program::Emit() {
	// TODO:
	// This is just a reference for you to get started
	//
	// You can use this as a template and create Emit() function
	// for individual node to fill in the module structure and instructions.

	Scp *globalScp = new Scp();
	IRGenerator::Instance().GetOrCreateModule("This_Class_Sucks");

	for (int i = 0; i < decls->NumElements(); i++) {
		decls->Nth(i)->Emit();
	}
	delete globalScp;

//	llvm::GlobalVariable gv = new llvm::GlobalVariable
//    IRGenerator irgen;
//    llvm::Module *mod = irgen.GetOrCreateModule("foo.bc");

//    // create a function signature
//    std::vector<llvm::Type *> argTypes;
//    llvm::Type *intTy = irgen.GetIntType();
//    argTypes.push_back(intTy);
//    llvm::ArrayRef<llvm::Type *> argArray(argTypes);
//    llvm::FunctionType *funcTy = llvm::FunctionType::get(intTy, argArray, false);
//
//    // llvm::Function *f = llvm::cast<llvm::Function>(mod->getOrInsertFunction("foo", intTy, intTy, (Type *)0));
//    llvm::Function *f = llvm::cast<llvm::Function>(mod->getOrInsertFunction("foo", funcTy));
//    llvm::Argument *arg = f->arg_begin();
//    arg->setName("x");
//
//    // insert a block into the function
//    llvm::LLVMContext *context = irgen.GetContext();
//    llvm::BasicBlock *bb = llvm::BasicBlock::Create(*context, "entry", f);
//
//    // create a return instruction
//    llvm::Value *val = llvm::ConstantInt::get(intTy, 1);
//    llvm::Value *sum = llvm::BinaryOperator::CreateAdd(arg, val, "", bb);
//    llvm::ReturnInst::Create(*context, sum, bb);
//
//    // write the BC into standard output
	// llvm::WriteBitcodeToFile(mod, llvm::outs());

	//below is useful for debugging
	//IRGenerator::Instance().GetOrCreateModule("This_Class_Sucks")->dump();
	llvm::WriteBitcodeToFile(IRGenerator::Instance().GetOrCreateModule("This_Class_Sucks"), llvm::outs());

}

void StmtBlock::Emit() {
	//if flag is set to false, create a new scope
	if (!Scp::curr->getShouldntCreateNewScope()) {
		bool isBreak = false;
		bool isContinue = false;
		bool inside_of_default_stmt = false;
		bool inside_of_case_stmt = false;
		bool has_break_inside_case = false;
		bool in_switch = false;

		isBreak = Scp::curr->getCanBreak();
		isContinue = Scp::curr->getCanContinue();
		inside_of_default_stmt = Scp::curr->getInside_of_default_stmt();
		inside_of_case_stmt = Scp::curr->getInside_of_case_stmt();
		has_break_inside_case = Scp::curr->getHas_break_inside_case();
		in_switch = Scp::curr->getIn_switch();

		Scp *scp = new Scp();
		scp->setCanBreak(isBreak);
		scp->setCanContinue(isContinue);
		scp->setHas_break_inside_case(has_break_inside_case);
		scp->setInside_of_default_stmt(inside_of_default_stmt);
		scp->setInside_of_case_stmt(inside_of_case_stmt);
		scp->setIn_switch(in_switch);
		// cout << "Creating new Scp in StmtBlock::CheckSub()\n";
		for (int i = 0; i < decls->NumElements(); i++) {
			decls->Nth(i)->Emit();
		}
		for (int i = 0; i < stmts->NumElements(); i++) {
			stmts->Nth(i)->Emit();
		}
		Scp::curr->getPrev()->setRetExists(scp->getRetExists());
		delete scp;
	} else { //else if flag is set to true, set the flag back to false
		Scp::curr->setShouldntCreateNewScope(false);
		// cout << "Not creating a new scope in StmtBlock::CheckSub()\n";
		for (int i = 0; i < decls->NumElements(); i++) {
			decls->Nth(i)->Emit();
		}
		for (int i = 0; i < stmts->NumElements(); i++) {
			stmts->Nth(i)->Emit();
		}
	}
}

StmtBlock::StmtBlock(List<VarDecl*> *d, List<Stmt*> *s) {
	Assert(d != NULL && s != NULL);
	(decls = d)->SetParentAll(this);
	(stmts = s)->SetParentAll(this);
}

void StmtBlock::PrintChildren(int indentLevel) {
	decls->PrintAll(indentLevel + 1);
	stmts->PrintAll(indentLevel + 1);
}

DeclStmt::DeclStmt(Decl *d) {
	Assert(d != NULL);
	(decl = d)->SetParent(this);
}

void DeclStmt::PrintChildren(int indentLevel) {
	decl->Print(indentLevel + 1);
}

ConditionalStmt::ConditionalStmt(Expr *t, Stmt *b) {
	Assert(t != NULL && b != NULL);
	(test = t)->SetParent(this);
	(body = b)->SetParent(this);
}

ForStmt::ForStmt(Expr *i, Expr *t, Expr *s, Stmt *b) :
		LoopStmt(t, b) {
	Assert(i != NULL && t != NULL && b != NULL);
	(init = i)->SetParent(this);
	step = s;
	if (s)
		(step = s)->SetParent(this);
}

void ForStmt::PrintChildren(int indentLevel) {
	init->Print(indentLevel + 1, "(init) ");
	test->Print(indentLevel + 1, "(test) ");
	if (step)
		step->Print(indentLevel + 1, "(step) ");
	body->Print(indentLevel + 1, "(body) ");
}

void ForStmt::Emit() {
	llvm::BasicBlock *beforeFor = IRGenerator::Instance().GetBasicBlock();
	llvm::BasicBlock *header = IRGenerator::Instance().createNewBasicBlock("for_header");
	header->moveAfter(beforeFor);

	llvm::BasicBlock *step_bb = IRGenerator::Instance().createNewBasicBlock("for_step");
	step_bb->moveAfter(header);
	llvm::BasicBlock *body_bb = IRGenerator::Instance().createNewBasicBlock("for_body");
	body_bb->moveAfter(step_bb);
	llvm::BasicBlock *footer = IRGenerator::Instance().createNewBasicBlock("for_footer");

	init->Emit();
	llvm::BranchInst::Create(header, IRGenerator::Instance().GetBasicBlock());
	llvm::CmpInst *cmp_inst_test = dynamic_cast<llvm::CmpInst*>(test->EmitExpr()); // Hope this works
	llvm::BranchInst::Create(body_bb, footer, cmp_inst_test, header);

	Scp *forScope = new Scp();
	forScope->setCanBreak(true);   //set break and continue bools
	// cout << "setCanBreak is now true in ForStmt::CheckSub()\n";
	forScope->setCanContinue(true);
	// cout << "Creating new Scp in ForStmt::CheckSub()\n";
	IRGenerator::Instance().setEndBb(footer);
	IRGenerator::Instance().setStartBb(step_bb);
	IRGenerator::Instance().SetBasicBlock(body_bb);

	body->Emit();
	IRGenerator::Instance().branchToBlock(step_bb);
	IRGenerator::Instance().SetBasicBlock(step_bb);
	step->Emit();
	llvm::BranchInst::Create(header, step_bb);
	// cout << "Checked body in ForStmt::CheckSub()\n";
	//setting retExists in case it was set to true at some point inside for scope
	Scp::curr->getPrev()->setRetExists(forScope->getRetExists());
	footer->moveAfter(IRGenerator::Instance().GetBasicBlock());
	IRGenerator::Instance().SetBasicBlock(footer);

	delete forScope;
}

void WhileStmt::PrintChildren(int indentLevel) {
	test->Print(indentLevel + 1, "(test) ");
	body->Print(indentLevel + 1, "(body) ");
}
void WhileStmt::Emit()
{
	Scp *whileScope = new Scp();
	whileScope->setCanBreak(true);   //set break and continue bools
	whileScope->setCanContinue(true);

	llvm::BasicBlock *beforeWhile = IRGenerator::Instance().GetBasicBlock();
	llvm::BasicBlock *headerCond = IRGenerator::Instance().createNewBasicBlock("while_header");
	headerCond->moveAfter(beforeWhile);

	llvm::BasicBlock *body_bb = IRGenerator::Instance().createNewBasicBlock("while_body");
	body_bb->moveAfter(headerCond);
	llvm::BasicBlock *footer = IRGenerator::Instance().createNewBasicBlock("while_footer");

	IRGenerator::Instance().setEndBb(footer);
	IRGenerator::Instance().setStartBb(headerCond);

	IRGenerator::Instance().SetBasicBlock(headerCond);//TODO is this needed?

	llvm::CmpInst *cmp_inst_test = dynamic_cast<llvm::CmpInst*>(test->EmitExpr());
	IRGenerator::Instance().SetBasicBlock(body_bb);
	body->Emit();
	IRGenerator::Instance().branchToBlock(headerCond);

	llvm::BranchInst::Create(headerCond, beforeWhile);
	llvm::BranchInst::Create(body_bb, footer, cmp_inst_test, headerCond);

	IRGenerator::Instance().SetBasicBlock(footer);

	Scp::curr->getPrev()->setRetExists(whileScope->getRetExists());
	delete whileScope;
}


IfStmt::IfStmt(Expr *t, Stmt *tb, Stmt *eb) :
		ConditionalStmt(t, tb) {
	Assert(t != NULL && tb != NULL); // else can be NULL
	elseBody = eb;
	if (elseBody)
		elseBody->SetParent(this);
}

void IfStmt::PrintChildren(int indentLevel) {
	if (test)
		test->Print(indentLevel + 1, "(test) ");
	if (body)
		body->Print(indentLevel + 1, "(then) ");
	if (elseBody)
		elseBody->Print(indentLevel + 1, "(else) ");
}

void IfStmt::Emit() {
	llvm::BasicBlock *else_basic_block = NULL;
	llvm::BasicBlock *init_basic_block = IRGenerator::Instance().GetBasicBlock();
	llvm::CmpInst *cmp_inst_test = dynamic_cast<llvm::CmpInst*>(test->EmitExpr()); // Hope this works
	Scp *ifScope = new Scp();
	llvm::BasicBlock *if_basic_block = IRGenerator::Instance().createNewBasicBlock("if_block");
	if_basic_block->moveAfter(init_basic_block);

	if (elseBody) {
		else_basic_block = IRGenerator::Instance().createNewBasicBlock("else_block");
		else_basic_block->moveAfter(if_basic_block);
	}
	llvm::BasicBlock *footer_basic_block = IRGenerator::Instance().createNewBasicBlock("if_footer_block");
	if (!elseBody) {
		llvm::BranchInst::Create(if_basic_block, footer_basic_block, cmp_inst_test, init_basic_block);
	} else {
		llvm::BranchInst::Create(if_basic_block, else_basic_block, cmp_inst_test, init_basic_block);
	}
	IRGenerator::Instance().SetBasicBlock(if_basic_block);
	body->Emit();
	IRGenerator::Instance().branchToBlock(footer_basic_block);
	footer_basic_block->moveAfter(IRGenerator::Instance().GetBasicBlock());
	delete ifScope;
	if (elseBody) {
		Scp *elseScope = new Scp();
		IRGenerator::Instance().SetBasicBlock(else_basic_block);
		elseBody->Emit();
		IRGenerator::Instance().branchToBlock(footer_basic_block);
		delete elseScope;
	}
	IRGenerator::Instance().SetBasicBlock(footer_basic_block);
	return;
}

ReturnStmt::ReturnStmt(yyltype loc, Expr *e) :
		Stmt(loc) {
	expr = e;
	if (e != NULL)
		expr->SetParent(this);
}

void ReturnStmt::PrintChildren(int indentLevel) {
	if (expr)
		expr->Print(indentLevel + 1);
}

void ReturnStmt::Emit() {
	// cout << "Entering ReturnStmt in ReturnStmt::CheckSub()\n";
	if (Scp::curr->getIn_switch()) {
		// cout << "In switch stmt inside of ReturnStmt::CheckSub()\n";
		if (Scp::curr->getInside_of_default_stmt()) {
			// cout << "In switch default inside of ReturnStmt::CheckSub()\n";
		}
		if (!Scp::curr->getHas_break_inside_case()) {
			// cout << "Case statement had a break in ReturnStmt::CheckSub()\n";
		}
		if (!Scp::curr->getHas_break_inside_case() && Scp::curr->getInside_of_default_stmt()) {
			Scp::curr->setRetExists(true);
		} else {
//	    	Type *expected = Scp::curr->getRetType();
//	        Type *given;
//	        if (NULL != expr) {
//	            given = expr->EmitExpr();
//	        }
//	        else {
//	            given = new Type("void");
//	        }
//
//	        // cout << "Expected: " << expected->getNameStr() << endl;
//	        // cout << "Given: " << given->getNameStr() << " in ReturnStmt::CheckSub()\n";
//	        if (expected->getNameStr() != given->getNameStr()) {
//	        	ReportError::ReturnMismatch(this, given, expected);
//	        }
			return;
		}
	} else {
		Scp::curr->setRetExists(true);
	}
	//compare the return type in the statement with the return type in the fndecl
	//if they are not equal, report error
//	Type *expected = Scp::curr->getRetType();
//
//	Type *given;
//	if (NULL != expr) {
//		given = expr->EmitExpr();
//	}
//	else {
//		given = new Type("void");
//	}
//
//	// cout << "Expected: " << expected->getNameStr() << endl;
//	// cout << "Given: " << given->getNameStr() << " in ReturnStmt::CheckSub()\n";
//	if (expected->getNameStr() != given->getNameStr()) {
//	    ReportError::ReturnMismatch(this, given, expected);
//	}

	if (NULL != expr) {
		llvm::ReturnInst::Create(*IRGenerator::Instance().GetContext(), expr->EmitExpr(), IRGenerator::Instance().GetBasicBlock());
	}
	return;
}
void ContinueStmt::Emit()
{
	llvm::BranchInst::Create(IRGenerator::Instance().getStartBb(), IRGenerator::Instance().GetBasicBlock());
}

void BreakStmt::Emit()
{
	llvm::BranchInst::Create(IRGenerator::Instance().getEndBb(), IRGenerator::Instance().GetBasicBlock());
}

SwitchLabel::SwitchLabel(Expr *l, Stmt *s) {
	Assert(l != NULL && s != NULL);
	(label = l)->SetParent(this);
	(stmt = s)->SetParent(this);
}

SwitchLabel::SwitchLabel(Stmt *s) {
	Assert(s != NULL);
	label = NULL;
	(stmt = s)->SetParent(this);
}

void SwitchLabel::PrintChildren(int indentLevel) {
	if (label)
		label->Print(indentLevel + 1);
	if (stmt)
		stmt->Print(indentLevel + 1);
}

SwitchStmt::SwitchStmt(Expr *e, List<Stmt *> *c, Default *d) {
	Assert(e != NULL && c != NULL && c->NumElements() != 0);
	(expr = e)->SetParent(this);
	(cases = c)->SetParentAll(this);
	def = d;
	if (def)
		def->SetParent(this);
}

void SwitchStmt::PrintChildren(int indentLevel) {
	if (expr)
		expr->Print(indentLevel + 1);
	if (cases)
		cases->PrintAll(indentLevel + 1);
	if (def)
		def->Print(indentLevel + 1);
}

void Case::Emit()
{
	llvm::BasicBlock* caseBB = IRGenerator::Instance().createNewBasicBlock("switch_case");
	IntConstant* intLabel = dynamic_cast<IntConstant*>(label); //TODO check if always int
	string valStr = std::to_string(intLabel->getValue());
	llvm::ConstantInt *cint = llvm::ConstantInt::get(*IRGenerator::Instance().GetContext(), llvm::APInt(32, llvm::StringRef(valStr), 10));
	IRGenerator::Instance().getSwitchInst()->addCase(cint, caseBB);
	IRGenerator::Instance().branchToBlock(caseBB);
	IRGenerator::Instance().SetBasicBlock(caseBB);
	stmt->Emit();
}

void Default::Emit()
{
	llvm::BasicBlock* defBB = IRGenerator::Instance().getSwitchInst()->getDefaultDest();
	IRGenerator::Instance().branchToBlock(defBB);
	IRGenerator::Instance().SetBasicBlock(defBB);
	stmt->Emit();

}

void SwitchStmt::Emit()
{
	bool defExists = false;
	int caseCount = 0;
//	if (def != NULL){
//		defExists = true;
//	}
	for (int i = 0; i < cases->NumElements(); i++)
	{
		if (dynamic_cast<Default*>(cases->Nth(i)) != NULL)
			defExists = true;
		else if (dynamic_cast<Case*>(cases->Nth(i)) != NULL)
			caseCount++;
	}

	llvm::BasicBlock *footer = IRGenerator::Instance().createNewBasicBlock("switch_footer");
	IRGenerator::Instance().setEndBb(footer);
	llvm::Value* valExpr = expr->EmitExpr();

	llvm::SwitchInst* switchInst;
	//llvm::BasicBlock *defaultBB = IRGenerator::Instance().createNewBasicBlock("switch_default");
	if(!defExists)
		switchInst = llvm::SwitchInst::Create(valExpr, IRGenerator::Instance().getEndBb(),
				caseCount,IRGenerator::Instance().GetBasicBlock());
	else
		switchInst = llvm::SwitchInst::Create(valExpr, IRGenerator::Instance().createNewBasicBlock("switch_default"),
				caseCount,IRGenerator::Instance().GetBasicBlock());

	IRGenerator::Instance().setSwitchBb(switchInst);

	for (int i = 0; i < cases->NumElements(); i++)
	{
		cases->Nth(i)->Emit();
	}
//	if(defExists)
//		def->Emit();
	//footer->moveAfter(IRGenerator::Instance().GetBasicBlock());
	IRGenerator::Instance().branchToBlock(footer);
	IRGenerator::Instance().SetBasicBlock(footer);

}
