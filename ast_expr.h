/* File: ast_expr.h
 * ----------------
 * The Expr class and its subclasses are used to represent
 * expressions in the parse tree.  For each expression in the
 * language (add, call, New, etc.) there is a corresponding
 * node class for that construct. 
 *
 * pp4: You will need to extend the Expr classes to implement
 * LLVM IR generation based on the AST expression/statement.
 */

// TODO add vector stuff to vardecl/varexpr
#ifndef _H_ast_expr
#define _H_ast_expr

#include "ast.h"
#include "ast_stmt.h"
#include "list.h"
#include "semantic_analyzer.h"
#include "errors.h"
#include "ast_decl.h"

void yyerror(const char *msg);

class Expr: public Stmt {
public:
	Expr(yyltype loc) :
			Stmt(loc) {
	}
	Expr() :
			Stmt() {
	}
	virtual llvm::Value* EmitExpr() = 0;
	virtual Type* getExprType() = 0;
    virtual llvm::Value *LLVMstore(llvm::Value *val) { return NULL; }
	void Emit() {
		EmitExpr();
	}
	virtual bool checkLVal() {
		return false;
	}
};

class ExprError: public Expr {
public:
	ExprError() :
			Expr() {
		yyerror(this->GetPrintNameForNode());
	}
	const char *GetPrintNameForNode() {
		return "ExprError";
	}
	llvm::Value* EmitExpr() {
		return NULL;
	}
	Type *getExprType() {
		return NULL;
	}

};

/* This node type is used for those places where an expression is optional.
 * We could use a NULL pointer, but then it adds a lot of checking for
 * NULL. By using a valid, but no-op, node, we save that trouble */
class EmptyExpr: public Expr {
public:
	const char *GetPrintNameForNode() {
		return "Empty";
	}
	void Emit() {
		cout << "Emit not implemented in EmptyExpr" << endl;
	}
	llvm::Value* EmitExpr() {
		return NULL;
	}
	Type *getExprType() {
		return NULL;
	}
};

class IntConstant: public Expr {
protected:
	int value;

public:
	IntConstant(yyltype loc, int val);
	const char *GetPrintNameForNode() {
		return "IntConstant";
	}
	void PrintChildren(int indentLevel);
	int getValue() {
		return value;
	}
	;
	llvm::Value* EmitExpr();
	Type *getExprType() {
		return new Type("int");
	}
};

class FloatConstant: public Expr {
protected:
	double value;

public:
	FloatConstant(yyltype loc, double val);
	const char *GetPrintNameForNode() {
		return "FloatConstant";
	}
	void PrintChildren(int indentLevel);
	double getValue() {
		return value;
	}
	;
	llvm::Value* EmitExpr();
	Type* getExprType() {
		return new Type("float");
	}

};

class BoolConstant: public Expr {
protected:
	bool value;

public:
	BoolConstant(yyltype loc, bool val);
	const char *GetPrintNameForNode() {
		return "BoolConstant";
	}
	void PrintChildren(int indentLevel);
	llvm::Value* EmitExpr() {
		return NULL;
	}
	Type* getExprType() {
		return new Type("bool");
	}
};

class VarExpr: public Expr {
protected:
	Identifier *id;

public:
	VarExpr(yyltype loc, Identifier *id);
    virtual llvm::Value *LLVMstore(llvm::Value *value) {
    	llvm::Value *val_from_scp = Scp::curr->getVarValue(this->id->getNameStr());
    	return new llvm::StoreInst(value, val_from_scp, false, IRGenerator::Instance().GetBasicBlock());
    }
	const char *GetPrintNameForNode() {
		return "VarExpr";
	}
	void PrintChildren(int indentLevel);
	Identifier* getIdentifier() {
		return id;
	}
	;
	Type* getExprType() {
		return Scp::curr->getNodeType(id);
	}
	llvm::Value* EmitExpr();
};

class Operator: public Node {
protected:
	char tokenString[4];

public:
	Operator(yyltype loc, const char *tok);
	string getTokStr() {
		return string(tokenString);
	}
	const char *GetPrintNameForNode() {
		return "Operator";
	}
	void PrintChildren(int indentLevel);
	char *getTokenChars() {
		return tokenString;
	}
};

class CompoundExpr: public Expr {
protected:
	Operator *op;
	Expr *left, *right; // left will be NULL if unary

public:
	CompoundExpr(Expr *lhs, Operator *op, Expr *rhs); // for binary
	CompoundExpr(Operator *op, Expr *rhs);             // for unary
	CompoundExpr(Expr *lhs, Operator *op);             // for unary
	void PrintChildren(int indentLevel);
	virtual llvm::Value* EmitExpr() = 0;
	virtual Type* getExprType() = 0;
};

class ArithmeticExpr: public CompoundExpr {
public:
	ArithmeticExpr(Expr *lhs, Operator *op, Expr *rhs) :
			CompoundExpr(lhs, op, rhs) {
	}
	ArithmeticExpr(Operator *op, Expr *rhs) :
			CompoundExpr(op, rhs) {
	}
	const char *GetPrintNameForNode() {
		return "ArithmeticExpr";
	}
	llvm::Value* EmitExpr();
	Type* getExprType() {
		return NULL;
	}
	;

};

class RelationalExpr: public CompoundExpr {
public:
	RelationalExpr(Expr *lhs, Operator *op, Expr *rhs) :
			CompoundExpr(lhs, op, rhs) {
	}
	const char *GetPrintNameForNode() {
		return "RelationalExpr";
	}
	llvm::Value* EmitExpr() {
		return IRGenerator::Instance().convertExprToLLVMCmp(left->EmitExpr(), op->getTokStr(), right->EmitExpr());
	}
	Type* getExprType() {
		return NULL;
	}
	;
};

class EqualityExpr: public CompoundExpr {
public:
	EqualityExpr(Expr *lhs, Operator *op, Expr *rhs) :
			CompoundExpr(lhs, op, rhs) {
	}
	const char *GetPrintNameForNode() {
		return "EqualityExpr";
	}
	llvm::Value* EmitExpr() {
		return IRGenerator::Instance().convertExprToLLVMCmp(left->EmitExpr(), op->getTokStr(), right->EmitExpr());
	}
	Type* getExprType() {
		return NULL;
	}
	;
};

class LogicalExpr: public CompoundExpr {
public:
	LogicalExpr(Expr *lhs, Operator *op, Expr *rhs) :
			CompoundExpr(lhs, op, rhs) {
	}
	LogicalExpr(Operator *op, Expr *rhs) :
			CompoundExpr(op, rhs) {
	}
	const char *GetPrintNameForNode() {
		return "LogicalExpr";
	}
	llvm::Value* EmitExpr() {
		return NULL;
	}
	Type* getExprType() {
		return NULL;
	}
};

class AssignExpr: public CompoundExpr {
public:
	AssignExpr(Expr *lhs, Operator *op, Expr *rhs) :
			CompoundExpr(lhs, op, rhs) {
	}
	const char *GetPrintNameForNode() {
		return "AssignExpr";
	}
	Type* getExprType() { return NULL; }
	llvm::Value* EmitExpr();
};

class PostfixExpr: public CompoundExpr {
public:
	PostfixExpr(Expr *lhs, Operator *op) :
			CompoundExpr(lhs, op) {
	}
	const char *GetPrintNameForNode() {
		return "PostfixExpr";
	}
	llvm::Value* EmitExpr();
	Type* getExprType() {
		return NULL;
	}
	;
};

class LValue: public Expr {
public:
	LValue(yyltype loc) :
			Expr(loc) {
	}
	bool checkLVal() {
		return true;
	}
};

class ArrayAccess: public LValue {
protected:
	Expr *base, *subscript;

public:
	ArrayAccess(yyltype loc, Expr *base, Expr *subscript);
	const char *GetPrintNameForNode() {
		return "ArrayAccess";
	}
	void PrintChildren(int indentLevel);
	llvm::Value* EmitExpr() {
		return NULL;
	}
	Type* getExprType() {
		return NULL;
	}
};

/* Note that field access is used both for qualified names
 * base.field and just field without qualification. We don't
 * know for sure whether there is an implicit "this." in
 * front until later on, so we use one node type for either
 * and sort it out later. */
class FieldAccess: public LValue {
protected:
	Expr *base;	// will be NULL if no explicit base
	Identifier *field;

public:
	FieldAccess(Expr *base, Identifier *field); //ok to pass NULL base
	const char *GetPrintNameForNode() {
		return "FieldAccess";
	}
	void PrintChildren(int indentLevel);
	llvm::Value* EmitExpr();
	Identifier *getIdentifier() {
		return field;
	}
	Type* getExprType() {
		return NULL;
	}
    virtual llvm::Value *LLVMstore(llvm::Value *value) {
    	llvm::Value *base_value = base->EmitExpr();
    	string swizzle = field->getNameStr();
    	if (swizzle.length() == 1) { // perfect
        	llvm::Value *swizzle_pos = IRGenerator::Instance().GetIntConst(IRGenerator::Instance().swizzleToInt(swizzle[0]));
    	    base_value = llvm::InsertElementInst::Create(base_value, value, swizzle_pos, "dude_vec_insert", IRGenerator::Instance().GetBasicBlock());
    	} else {
    		for (unsigned int i = 0; i < swizzle.length(); i++) {
    			llvm::Value *curr_position = IRGenerator::Instance().GetIntConst(i);
    			llvm::Value *curr_item = llvm::ExtractElementInst::Create(value, curr_position, string(1,swizzle[i]),  IRGenerator::Instance().GetBasicBlock());
    			llvm::Value *curr_letter_to_num = IRGenerator::Instance().GetIntConst(IRGenerator::Instance().swizzleToInt((swizzle[i])));
    			base_value = llvm::InsertElementInst::Create(base_value, curr_item, curr_letter_to_num, "dude_vec_insert", IRGenerator::Instance().GetBasicBlock());
    		}
    	}
    	return base->LLVMstore(base_value);
    }
};

/* Like field access, call is used both for qualified base.field()
 * and unqualified field().  We won't figure out until later
 * whether we need implicit "this." so we use one node type for either
 * and sort it out later. */
class Call: public Expr {
protected:
	Expr *base;	// will be NULL if no explicit base
	Identifier *field;
	List<Expr*> *actuals;

public:
	Call() :
			Expr(), base(NULL), field(NULL), actuals(NULL) {
	}
	Call(yyltype loc, Expr *base, Identifier *field, List<Expr*> *args);
	const char *GetPrintNameForNode() {
		return "Call";
	}
	void PrintChildren(int indentLevel);
	llvm::Value* EmitExpr() {
		return NULL;
	}
	Type* getExprType() {
		return NULL;
	}
};

class ActualsError: public Call {
public:
	ActualsError() :
			Call() {
		yyerror(this->GetPrintNameForNode());
	}
	const char *GetPrintNameForNode() {
		return "ActualsError";
	}
};

Type *isValidFullExpr(Type *t1, Operator *op, Type *t2);
bool checkLogicOp(Type *t1, Type *t2);
bool checkEqOp(Type *t1, Type *t2);
Type *checkAssignOrArithOp(Type *t1, Operator *op, Type *t2);
bool checkRelOp(Type *t1, Type *t2);
bool checkArithOp(Type *t1, Type *t2);
bool isArithOp(Operator *op);
bool isAssignOp(Operator *op);
bool isRelOp(Operator *op);
bool isLogicOp(Operator *op);
bool isEqOp(Operator *op);
bool isValidUnaryExpr(Type *t);
bool checkBoolType(Type *t);
bool checkMatType(Type *t);
bool checkVectorType(Type *t);
bool checkScalarType(Type *t);
bool isOversizedVector(Identifier *swizzle);
bool isSwizzleOutOfBound(Identifier *swizzle, string swizzle_size);
bool isInvalidSwizzle(Identifier *swizzle);

#endif
