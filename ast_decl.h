/* File: ast_decl.h
 * ----------------
 * In our parse tree, Decl nodes are used to represent and
 * manage declarations. There are 4 subclasses of the base class,
 * specialized for declarations of variables, functions, classes,
 * and interfaces.
 *
 * pp4: You will need to extend the Decl classes to implement
 * code emitter to generate LLVM global varaible declaration and
 * functions.
 *
 */

#ifndef _H_ast_decl
#define _H_ast_decl

#include "ast.h"
#include "list.h"
#include "ast_type.h"
#include "irgen.h"

//class Type;
//class NamedType;
//class Identifier;
class Stmt;

void yyerror(const char *msg);

class Decl: public Node {
protected:
	Identifier *id;

public:
	Decl() :
			id(NULL) {
	}
	Decl(Identifier *name);
	friend ostream &operator<<(ostream &out, Decl *d) {
		return out << d->id;
	}
	string getIDName() {
		return string(id->getName());
	}
	Identifier *GetIdentifier() const {
		return id;
	}
	virtual void Emit() = 0;

};

class VarDecl: public Decl {
protected:
	Type *type;

public:
	VarDecl() :
			type(NULL) {
	}
	VarDecl(Identifier *name, Type *type);
	const char *GetPrintNameForNode() {
		return "VarDecl";
	}
	Type *getType() {
		return type;
	}
	void PrintChildren(int indentLevel);
	void Emit();
};

class VarDeclError: public VarDecl {
public:
	VarDeclError() :
			VarDecl() {
		yyerror(this->GetPrintNameForNode());
	}
	;
	const char *GetPrintNameForNode() {
		return "VarDeclError";
	}
};

class FnDecl: public Decl {
protected:
	List<VarDecl*> *formals;
	Type *returnType;
	Stmt *body;

public:
	FnDecl() :
			Decl(), formals(NULL), returnType(NULL), body(NULL) {
	}
	FnDecl(Identifier *name, Type *returnType, List<VarDecl*> *formals);
	void SetFunctionBody(Stmt *b);
	const char *GetPrintNameForNode() {
		return "FnDecl";
	}
	void PrintChildren(int indentLevel);
	List<VarDecl*>* getFormals() {
		return formals;
	}
	void Emit();
	Type* getReturnType() {
		return returnType;
	}
};

class FormalsError: public FnDecl {
public:
	FormalsError() :
			FnDecl() {
		yyerror(this->GetPrintNameForNode());
	}
	const char *GetPrintNameForNode() {
		return "FormalsError";
	}
};

#endif
